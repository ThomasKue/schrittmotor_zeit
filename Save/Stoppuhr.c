/******************************************************************************/
/*   (C) Copyright HTL - HOLLABRUNN  2009-2009 All rights reserved. AUSTRIA   */ 
/*                                                                            */ 
/* File Name:   Lauflicht_Interrupt.c                                         */
/* Autor: 		Thomas K�hleitner                                               */
/* Version: 	V1.00                                                           */
/* Date: 		11/03/2012                                                        */
/* Description: Zeitanzeige Schrittmotorprogramm                              */
/******************************************************************************/
/* History: 	V1.00  creation										                              */
/******************************************************************************/
#include <armv10_std.h>
#include <stm32f10x.h>


/*------------------------------ Function Prototypes -------------------------*/
void set_HSE_32MHz(void);
void TIM3_Config(void);
void NVIC_init(char position, char priority);
void TIM3_IRQHandler(void);

/*------------------------------ Static Variables-------------------------*/
static int ms10=0;

/******************************************************************************/
/*           Interrupt Service Routine  Timer3 (General Purpose Timer)        */
/******************************************************************************/
void TIM3_IRQHandler(void)	//Timer 3, l�st alle 10ms aus
{
	TIM3->SR &=~0x01;	 //Interrupt pending bit l�schen (Verhinderung eines nochmaligen Interrupt-ausl�sens)
	ms10++; // +1x10ms
}

/******************************************************************************/
/*                     Initialization Timer3 (General Purpose Timer)          */
/******************************************************************************/
static void TIM3_Config(void)
{	
	/*---------------------- Timer 3 konfigurieren -----------------------*/
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;	//Timer 3 Clock enable
	TIM3->SMCR = 0x0000;	 
	TIM3->CR1  = 0x0000;	// Auswahl des Timer Modus: Upcounter --> Z�hlt: von 0 bis zum Wert des Autoreload-Registers

	/* T_INT = 126,26ns, Annahme: Presc = 150 ---> Auto Reload Wert = 52801 (=0xCE41) --> 1s Update Event*/
	TIM3->PSC = 10;		//Wert des prescalers (Taktverminderung)
	TIM3->ARR = 0xFFFF;		//Auto-Reload Wert = Maximaler Zaehlerstand des Upcounters
	TIM3->RCR = 0;			//Repetition Counter deaktivieren

	/*--------------------- Interrupt Timer 3 konfigurieren ---------------------*/
	TIM3->DIER = 0x01;	   //enable Interrupt bei einem UEV (�berlauf / Unterlauf)
	NVIC_init(TIM3_IRQn,2);	   //Enable Timer 3 Update Interrupt, Priority 2

	/*-------------------------- Timer 3 Starten -------------------------------*/
    TIM3->CR1 |= 0x0001;   //Counter-Enable bit setzen

}

/******************************************************************************/
/*                   NVIC_init(char position, char priority)    			  			*/
/* Funktion:                                                                  */
/*   �bernimmt die vollst�ndige initialisierung eines Interrupts  im Nested   */
/*   vectored Interrupt controller (Priorit�t setzen, Ausl�sen verhindern,    */
/*   Interrupt enablen)                                                       */
/* �bergabeparameter: "position" = 0-67 (Nummer des Interrupts)               */
/*                    "priority": 0-15 (Priorit�t des Interrupts)		     		  */
/******************************************************************************/
static void NVIC_init(char position, char priority) 
{	
	NVIC->IP[position]=(priority<<4);	//Interrupt priority register: Setzen der Interrupt Priorit�t
	NVIC->ICPR[position >> 0x05] |= (0x01 << (position & 0x1F));//Interrupt Clear Pendig Register: Verhindert, dass der Interrupt ausl�st sobald er enabled wird 
	NVIC->ISER[position >> 0x05] |= (0x01 << (position & 0x1F));//Interrupt Set Enable Register: Enable interrupt
} 

/******************************************************************************/
/*                   						set_HSE_72MHz 								   			  			*/
/* Funktion:                                                                  */
/*																																						*/
/* �bergabeparameter: "position" = 0-67 (Nummer des Interrupts)               */
/*                    "priority": 0-15 (Priorit�t des Interrupts)		     		  */
/******************************************************************************/
void set_HSE_32MHz(void){
FLASH->ACR = 0x12;
RCC->CR |= RCC_CR_HSEON;
while ((RCC->CR & RCC_CR_HSERDY) == 0); 
RCC->CFGR |= RCC_CFGR_PLLMULL4;
RCC->CFGR |= RCC_CFGR_ADCPRE_DIV6;
RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
RCC->CFGR |= RCC_CFGR_PLLSRC;
RCC->CR |= RCC_CR_PLLON;
while ((RCC->CR & RCC_CR_PLLRDY) == 0);
//RCC->CFGR |= RCC_CFGR_SW_HSE; //HSE = Systemclock
RCC->CFGR |= RCC_CFGR_SW_PLL; 
while ((RCC->CFGR & RCC_CFGR_SWS_PLL) == 0);
while ((RCC->CFGR & RCC_CFGR_SWS) != ((RCC->CFGR<<2) & RCC_CFGR_SWS));
RCC->BDCR |=RCC_BDCR_LSEON;
}


/******************************************************************************/
/*                                MAIN function                               */
/******************************************************************************/
int main (void) 
{
int zehntel=0, sek=0, min=0, std=0;
char buffer[30];
	
set_HSE_32MHz();
TIM3_Config();	
lcd_init();
lcd_clear();

do{
	if((ms10%10==0)&&(zehntel!=ms10))
	{
		zehntel=ms10;
		if(zehntel>99)
		{
			sek++;
			zehntel=0;
			ms10=0;
		}
		if(sek>59)
		{
			min++;
			sek=0;
		}
		if(min>59)
		{
			std++;
			min=0;
		}
		if(std>23)
		{
			std=0;
		}
		
		lcd_set_cursor(0,0);
		sprintf(&buffer[0], "%02d:%02d:%02d:%d", std, min, sek, zehntel);
		lcd_put_string(&buffer[0]);
	}
} while (1);

}
