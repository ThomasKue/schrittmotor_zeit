/******************************************************************************/
/*   (C) Copyright HTL - HOLLABRUNN  2009-2009 All rights reserved. AUSTRIA   */ 
/*                                                                            */ 
/* Project Name:	Parabol Schrittmotor                                        */
/* Autor: 				K�hleitner und M�llauer                                     */
/* Version: 			V1.00                                                       */
/* Description: 	Erkennen von Flanken, Ausagabe eine HEX Zahl                */
/******************************************************************************/
/* History:                                                                   */
/* Creation:		19.04.2018                                                    */
/******************************************************************************/
/* Zeitaufwand:                                                               */
/* Creation:		Muellauer & K�hleitner                                        */
/*							Main.c erstellt                                               */
/******************************************************************************/
#include <stm32f10x.h>
#include <ARMV10_STD.h>
#include <string.h>
/*---------------------------Prototypes---------------------------------------*/
void 	nec_init_port(void);
/*---------------------------Globale Variable---------------------------------*/

/*---------------------------Bit Banding Alias Adesses------------------------*/
#define GPIOA_IDR GPIOA_BASE + 2*sizeof(uint32_t)	 // Calc peripheral address GPIOB ODR
#define BITBAND_PERI(a,b) ((PERIPH_BB_BASE + (a-PERIPH_BASE)*32 + (b*4)))  
#define SER_DATA  *((volatile unsigned long *)(BITBAND_PERI(GPIOA_IDR,9)))	// PA9 
#define SW0  *((volatile unsigned long *)(BITBAND_PERI(GPIOA_IDR,0)))	// PA0
/******************************************************************************/
/*                                Hauptprogramm                               */
/******************************************************************************/
int main (void) 
{
	/*--Initialisieren--*/
	int start,repeat;
	nec_init_port();
	/*----Init-Ende-----*/
	
	/*------Start-------*/
	do
	{
		
	}while(1);
	/*------Ende--------*/
}

/******************************************************************************/
/*            U N T E R P R O G R A M M:    nec_init_port                     */
/*                                                                            */
/* Aufgabe:   Initialisiert Portleitung PA0 f�r Schalter  SW0                 */
/* Input:                                                                     */
/* return:	                                                                  */
/******************************************************************************/
void nec_init_port(void) 
{
	int temp;

	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;       
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;       

	//Beim Usart1 Ports umkonfiguieren
	temp 	= GPIOA->CRH;
	temp 	&=0xFFFFFF0F;																// PA15-PA8 Konfigurationsbits l�schen
	temp 	|= 0x80;																		// PA9 auf Input Pull-up
	GPIOA->CRH 		= temp;															// Input Pull-up
	
	// Schalter auf Led /Schalterplatine
	temp 	= GPIOA->CRL;
	temp 	&= 0x00000000;															// PA7-PA0 Konfigurationsbits l�schen
	temp 	|= 0x88888888;															// PA7-PA0 als Input definieren
	GPIOA->CRL 		= temp;
	
	//LEDs auf Led / Schalterplatine
	temp 	= GPIOB->CRH;
	temp 	&= 0x00000000;															// PB15-PB8 Konfigurationsbits l�schen	
	temp 	|= 0x33333333;															// PB15-PB8 als Push Pull Output definieren
	GPIOB->CRH 		= temp;
	GPIOA->BSRR 	= 0x02FF;
}
